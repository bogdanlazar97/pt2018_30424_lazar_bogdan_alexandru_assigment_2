package gui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import background.Client;
import background.Organizer;
import background.Que;

public class GSim extends JFrame {
	public static JLabel Q1lbl;
	public static JLabel Q2lbl;
	public static JLabel Q3lbl;


	public static JTextField Q1t = new JTextField(" ");
	public static JTextField Q2t = new JTextField(" ");
	public static JTextField Q3t = new JTextField(" ");

	
	
	
	int nrofQueues =3;
	int minArrival;
	int maxArrival;
	int minService;
	int maxService;
	
	public GSim(int nrofQueues,int minArrival,int maxArrival,int minService,int maxService) {
		this.nrofQueues = nrofQueues;
		this.minArrival = minArrival;
		this.maxArrival = maxArrival;
		this.minService = minService;
		this.maxService = maxService;
		initializeface2();
	}
	private void initializeface2() {
		setTitle("Simulate Aplication");
		setSize(250,300);
		setLocationRelativeTo(null);
		setLayout(null);
		setResizable(false);
		JButton Start = new JButton("Start");
		Start.setBounds(70,200,100,60);
		Q1lbl = new JLabel("Queue1");
		Q2lbl = new JLabel("Queue2");
		Q3lbl = new JLabel("Queue3");
		
		Q1lbl.setBounds(30,20,100,20);
		Q2lbl.setBounds(30,80,100,20);
		Q3lbl.setBounds(30,140,100,20);
		
		
		Q1t = new JTextField("");
		Q2t = new JTextField("");
		Q3t = new JTextField("");
		
		
		
		Q1t.setBounds(90,20,100,20);
		Q2t.setBounds(90,80,100,20);
		Q3t.setBounds(90,140,100,20);
		
		Q1t.setEditable(false);
		Q2t.setEditable(false);
		Q3t.setEditable(false);
		
		
		add(Start);

		
		switch(nrofQueues){
		case 1: {
			add(Q1t);
			add(Q1lbl);
			break;
		}
			case 2:{
				add(Q1t);
				add(Q2t);
				add(Q1lbl);
				add(Q2lbl);
				break;
				}
			case 3:{
				add(Q1t);
				add(Q2t);
				add(Q3t);
				add(Q1lbl);
				add(Q2lbl);
				add(Q3lbl);
				break;
			}
			
			
			default:{
				add(Q1t);
				add(Q2t);
				add(Q3t);
				break;
			}
		}
		Start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start();
			}
		});
	}
	private void start() {
		SwingWorker<Void,Client> worker = new SwingWorker<Void, Client>(){

			@Override
			protected Void doInBackground() throws Exception {
				Que q[] = new Que[nrofQueues];
				for(int i = 0;i<nrofQueues;i++) {
					q[i] = new Que("Queue" + Integer.toString(i),i);
					q[i].start();
				}
				Organizer s = new Organizer(nrofQueues,q,minArrival,maxArrival,minService,maxService);
				s.start();
				
				return null;
			}

			@Override
			protected void process(List<Client> chunks) {
				
				super.process(chunks);
			}
			
			
		};
		
		worker.execute();
	}
}
