package gui;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import background.Que;
import background.Organizer;


public class GValue extends JFrame {
	static GValue sim2 = new GValue();
	int nrOfQueues = 0;
	static int nrOfClients;
	public static int maxService;
	public static int minService;
	int maximumArrival;
	int minimumArrival;
	public GValue() {
		initializeInterface();
	}
	
	public int getminimumService() {
		return minService;
	}
	public static int getClients() {
		return nrOfClients;
	}
	
	private void initializeInterface() {
		 setTitle("Initializer");
		 setSize(400,170);
		 setLocationRelativeTo(null);
		 setLayout(null);
		 setResizable(false);
		 JTextField minSInp = new JTextField("");
		 JTextField maxSInp = new JTextField("");
		 JLabel maxSLabel = new JLabel("Max service time");
		 JLabel minSLabel = new JLabel("Min service time");
		 JLabel nrofclients = new JLabel("Nr of clients");
		 JTextField nrofclientst= new JTextField("");

		 JLabel nrOfQLabel = new JLabel("Input the number of Ques 1-3:");
		 JTextField nrOfQInp = new JTextField("");
		 JButton start = new JButton("Start");
		 
		
		 nrofclients.setBounds(20, 70, 600, 20);
		 nrofclientst.setBounds(130, 70, 60, 20);
		 
		 
		 
		 maxSLabel.setBounds(20,40,110,20);
		 maxSInp.setBounds(130,40,60,20);
		 minSLabel.setBounds(20,10,110,20);
		 minSInp.setBounds(130,12,60,20);
		
		 nrOfQLabel.setBounds(20,100,210,20);
		 nrOfQInp.setBounds(220,100,60,20);
		 
		 start.setBounds(250,50,110,20);
		 
		 setDefaultCloseOperation(EXIT_ON_CLOSE);
		 
		 add(nrofclientst);
		 add(nrofclients);
		 add(start);
		 add(nrOfQInp);
		 add(nrOfQLabel);
		 add(maxSLabel);
		 add(minSLabel);
		 add(minSInp);
		 add(maxSInp);
		 
		 start.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
				 try {
				 nrOfQueues=Integer.parseInt(nrOfQInp.getText());
				 maxService=Integer.parseInt(maxSInp.getText());
				 minService=Integer.parseInt(minSInp.getText());
				 nrOfClients=Integer.parseInt(nrofclientst.getText());
				 maximumArrival=2;
				 minimumArrival=1;
				 }catch(Exception e1){
					 JOptionPane.showMessageDialog(null, "Invalid enter again", "WARNING", JOptionPane.WARNING_MESSAGE);
				 }
				 if(maxService<=minService) {
					
					 maxService =5;
					 minService =3;
					 JOptionPane.showMessageDialog(null, "Minimum and Maximum are inverted", "WARNING", JOptionPane.WARNING_MESSAGE);
				 }
				 else if(maximumArrival<=minimumArrival) {
					 
					 maximumArrival=5;
					 minimumArrival=3;
					 JOptionPane.showMessageDialog(null, "Minimum and Maximum are inverted", "WARNING", JOptionPane.WARNING_MESSAGE);
				 }
				 else if(nrOfQueues<=1 || nrOfQueues>3) {
					 JOptionPane.showMessageDialog(null, "Check Ques", "WARNING", JOptionPane.WARNING_MESSAGE);
				 }else {
					GSim sim = new GSim(nrOfQueues,minimumArrival,maximumArrival,minService,maxService);		
						sim.setVisible(true);
						sim2.setVisible(false);
				 }
				 
			 }
		 });
	}
	public int getmaximumService() {
		return maxService;
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(()->{ 
			sim2.setVisible(true);
		});
	}
}
