package background;
import java.util.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

import gui.GSim;

public class Organizer extends Thread{
	private Que queue[];
	private int nrOfQueues;
	private int nrOfClients;
	private int maxArriv;
	private int minArriv;
	public static int minService;
	private int maxService;
	private int serviceMi;
	private int serviceMa; 
	public int totalservice=0;
	public static int arr1[]=new int[200];
	public static int arr2[]=new int[200];
	public static int arr3[]=new int[200];
	public static int arr4[]=new int[200];
	public static  int arr5[]=new int[200];
	public static  int arr6[]=new int[200];
	public static int arr7[]=new int[200];
	public static  int arr8[]=new int[200];
	public static  int arr9[]=new int[200];
	public static  int arr10[]=new int[200];
	public static int z = 0;
	int max =1;
	public static int j =0;
	public Organizer(int nrOfQueues,Que queue[],int minArriv,int maxArriv,int minService,int maxService) {
		Random random = new Random();
		this.nrOfClients =gui.GValue.getClients(); 
		this.nrOfQueues = nrOfQueues;
		this.queue = new Que[nrOfQueues];
		this.maxArriv = maxArriv*1000;
		this.minArriv = minArriv*1000;
		this.maxService = maxService*1000;
		this.minService = minService*1000;
		this.serviceMa = maxService;
		this.serviceMi = minService;
		for(int i = 0;i < nrOfQueues;i++) {
			this.queue[i] = queue[i];
		}
	}
	
	private int smallestQue() {
		int in = 0;
		try {
			long min = queue[0].queueWait();
			for(int i = 0;i<nrOfQueues;i++) {
				long currentQueue = queue[i].queueWait();
				if(currentQueue < min) {
					min = currentQueue;
					in = i;
				}
			}
		}catch(InterruptedException e) {
		//	System.out.println(e.toString());
		}
		return in;
	}

	public static int index() {
		return z;
	}
	public static int[] arr1() {
		return arr1;
	}
	public static int[] arr2() {
		return arr2;
	}
	public static int[] arr3() {
		return arr3;
	}
	public static int[] arr4() {
		return arr4;
	}
	public static int[] arr5() {
		return arr5;
	}
	public static int[] arr6() {
		return arr6;
	}
	public static int[] arr7() {
		return arr7;
	}
	public static int[] arr8() {
		return arr8;
	}
	public static int[] arr9() {
		return arr9;
	}
	public static int[] arr10() {
		return arr10;
	}
	
	
	    public void log(String message) { 
	      PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter("clientsinput.txt", true), true);
			out.write(message);
		      out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      
	    }
	    public void logdata(String message) { 
		      PrintWriter out;
			try {
				File file = new File("C:\\Users\\Bogdan\\eclipse-workspace\\ptfif\\data.txt");
		         
		        if(file.delete())
		        {
		           // System.out.println("File deleted successfully");
		        }
		        else
		        {
		           // System.out.println("Failed to delete the file");
		        }
				out = new PrintWriter(new FileWriter("data.txt", true), true);
				out.write(message);
			      out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      
		    }
	    public void logdata2() { 
		      PrintWriter out;
			try {
				
				File file = new File("C:\\Users\\Bogdan\\eclipse-workspace\\ptfif\\peak.txt");
		         
		        if(file.delete())
		        {
		        	//System.out.println("File deleted successfully");
		        }
		        else
		        {
		           // System.out.println("Failed to delete the file");
		        }
		        
	            out = new PrintWriter(new FileWriter("peak.txt", true), true);
				out.write( LocalDateTime.now().toString());
			      out.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		      
		    }
	
	
	public void run() {
		try {
			sleep(2000);
			
			System.out.println(nrOfClients);
			while(z<nrOfClients) {
				z++;
				int smallestQue = smallestQue();
				Client c = new Client(maxArriv,minArriv,minService,maxService,z,smallestQue);
				String q1 =GSim.Q1t.getText();
				String q2 = GSim.Q2t.getText();
				String q3 =GSim.Q3t.getText();
				
					if (max<q1.length()+q2.length()+q3.length()) {
					
					logdata2();
					max =q1.length()+q2.length()+q3.length();
				}

				log("Client: "+Long.toString(c.ID)+" added to Queue "+Integer.toString(smallestQue)+ " at "+ LocalDateTime.now() );
				
				
				switch(smallestQue) {
				case 0:{
					Random ran = new Random();
					int x = ran.nextInt(((gui.GValue.maxService-gui.GValue.minService)+1) + gui.GValue.minService);
					totalservice+= x;
					arr1[z+x]=1;
					String data ="Average service time : "+Integer.toString(totalservice/10)+"\n Minimum waiting time : "+gui.GValue.minService+"\n Maximum Waiting time :"+gui.GValue.maxService
							;
					logdata(data);
					String s = GSim.Q1t.getText();
					s=s+ " "+c.ID;
		
					
					GSim.Q1t.setText(s);
					
					break;
				}
				case 1:{
					Random ran = new Random();
					int x = ran.nextInt(((gui.GValue.maxService-gui.GValue.minService)+1) + gui.GValue.minService);
					totalservice+= x;
					arr2[z+x]=1;
					String data ="Average service time : "+Integer.toString(totalservice/10)+"\nMinimum waiting time :"+gui.GValue.minService+"\nMaximum Waiting time :"+gui.GValue.maxService
							;
					logdata(data);
					String s = GSim.Q2t.getText();
					GSim.Q2t.setText(s + " " + c.ID);
					break;
				}
				case 2:{
					Random ran = new Random();
					int x = ran.nextInt(((gui.GValue.maxService-gui.GValue.minService)+1) + gui.GValue.minService);
					arr3[z+x]=1;
					totalservice+= x;
					String data ="Average service time : "+Integer.toString(totalservice/10)+"\nMinimum waiting time :"+gui.GValue.minService+"\nMaximum Waiting time :"+gui.GValue.maxService
							;
					logdata(data);
					String s = GSim.Q3t.getText();
					GSim.Q3t.setText(s + " " + c.ID);
					break;
				}
				}

				queue[smallestQue].addClient(c);
				int arrivalTime = c.getArrival();
				sleep(arrivalTime); 
			}
			
		}catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}
}
