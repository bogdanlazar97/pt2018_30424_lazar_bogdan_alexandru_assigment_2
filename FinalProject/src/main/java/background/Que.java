package background;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import gui.GSim;
import background.Organizer;
import background.Client;

public class Que extends Thread{
	private static  Vector<Client> que = new Vector<Client>();
	int queT=0;
	int totalT=0;
	int waitTime=0;
	int service=0;
	int queueID=0;
	int max=1;
	public Que(String name,int queueID) {
		setName(name);
		this.queueID = queueID;
	}
	public void run() {
		try {
			int i=0;
			while(true) {
				try {
					
				deleteClient(Organizer.index(),Organizer.arr1(),Organizer.arr2(),Organizer.arr3(),Organizer.arr4(),Organizer.arr5(),Organizer.arr6(),Organizer.arr7(),Organizer.arr8(),Organizer.arr9(),Organizer.arr10());
				
				sleep(Organizer.index()*1000);
				}catch(Exception e) {
					sleep(500);
				}
			}
		}catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}

	public synchronized void addClient(Client client) throws InterruptedException
	{
		
		que.addElement(client);
		queT += client.serviceT;
		client.leaveTime = queT;
		
		notifyAll();
	}
	 public void log(String message) { 
	      PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter("clientsoutput.txt", true), true);
			out.write(message);
		      out.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	      
	    }
	 
	
	
	public synchronized void deleteClient(int index,int arr1[],int arr2[],int arr3[],int arr4[],int arr5[],int arr6[],int arr7[],int arr8[],int arr9[],int arr10[]) throws InterruptedException{
		String q1 =GSim.Q1t.getText();
		String q2 = GSim.Q2t.getText();
		String q3 =GSim.Q3t.getText();
		
		
		
			if(q1.length()<=2) {
				GSim.Q1t.setText("");
				log("Client has left at "+LocalDateTime.now() );
				
			}else if (Character.isDigit(q1.charAt(1))) {
		String numbers1 = q1.substring(q1.length() - q1.length()+3, q1.length());
		GSim.Q1t.setText(numbers1);
		log("Client has left at"+LocalDateTime.now() );
			}else {
				log("Client has left at"+LocalDateTime.now() );
				String numbers1 = q1.substring(q1.length() - q1.length()+2, q1.length());
				GSim.Q1t.setText(numbers1);
			}
		
		
			if(q2.length()<=2) {
				GSim.Q2t.setText("");
				log("Client has left at"+LocalDateTime.now() );
			}else if (Character.isDigit(q2.charAt(1))) {
				String numbers2 = q2.substring(q2.length() - q2.length()+3, q2.length());
				GSim.Q2t.setText(numbers2);
				log("Client has left at"+LocalDateTime.now() );
					}else {
						String numbers2 = q2.substring(q2.length() - q2.length()+2, q2.length());
						GSim.Q2t.setText(numbers2);
						log("Client has left at"+LocalDateTime.now() );
					}
			
		
			if(q3.length()<=2) {
				GSim.Q3t.setText("");
				log("Client has left at"+LocalDateTime.now() );
			}else if (Character.isDigit(q3.charAt(1))) {
				String numbers3 = q3.substring(q3.length() - q3.length()+3, q3.length());
				GSim.Q3t.setText(numbers3);
				log("Client has left at"+LocalDateTime.now() );
					}else {
						String numbers3 = q3.substring(q3.length() - q3.length()+2, q3.length());
						GSim.Q3t.setText(numbers3);
						log("Client has left at"+LocalDateTime.now() );
					}
		
		
	}
		
		
	
	
	public synchronized long queueWait() throws InterruptedException{
		notifyAll();
		return queT;
	}
}
