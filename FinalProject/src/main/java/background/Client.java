package background;



import java.util.Random;

public class Client {
	int arrivalT;
	long ID;
	int serviceT;
	int arriveQueueT = 0;
	int leaveTime = 0;
	int Queue;
	boolean printed = false;
	
	public Client(int maxAT,int minAT,int minST,int maxST,long ID,int Queue){
		Random r = new Random();
		this.ID = ID;	
		this.arrivalT = r.nextInt(maxAT + 1 - minAT) + minAT;
		
		this.Queue = Queue;
		this.serviceT = r.nextInt(maxST + 1 - minST) + minST;
	}
	
	public long getID() {
		return ID;
	}
	
	public int getArrival() {
		return arrivalT;
	}
	
	public int getService() {
		return serviceT;
	}
	
	public int getLeave() {
		return leaveTime;
	}
	public String toString() {
		return(Long.toString(ID)+" "+ arrivalT + " "+serviceT);
	}
	
	public void changeBool() {
		printed = true;
	}
}
